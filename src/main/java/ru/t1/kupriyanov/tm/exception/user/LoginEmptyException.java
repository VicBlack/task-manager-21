package ru.t1.kupriyanov.tm.exception.user;

public class LoginEmptyException extends AbstractUserException {

    public LoginEmptyException() {
        super("Error! Login is empty!");
    }

}

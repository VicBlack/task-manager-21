package ru.t1.kupriyanov.tm.exception.user;

public class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error! User does not exist!");
    }

}

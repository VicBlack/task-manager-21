package ru.t1.kupriyanov.tm.exception.field;

import ru.t1.kupriyanov.tm.exception.AbstractException;

public class AbstractFiledException extends AbstractException {

    public AbstractFiledException() {
    }

    public AbstractFiledException(final String message) {
        super(message);
    }

    public AbstractFiledException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFiledException(final Throwable cause) {
        super(cause);
    }

    public AbstractFiledException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
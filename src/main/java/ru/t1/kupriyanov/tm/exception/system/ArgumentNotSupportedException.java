package ru.t1.kupriyanov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public ArgumentNotSupportedException(final String command) {
        super("Error! Argument " + command + " is not supported...");
    }

}

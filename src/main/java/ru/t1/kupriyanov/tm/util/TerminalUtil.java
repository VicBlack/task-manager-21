package ru.t1.kupriyanov.tm.util;

import ru.t1.kupriyanov.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws NumberIncorrectException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}

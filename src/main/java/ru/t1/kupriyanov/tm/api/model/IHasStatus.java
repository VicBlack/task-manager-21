package ru.t1.kupriyanov.tm.api.model;

import ru.t1.kupriyanov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}

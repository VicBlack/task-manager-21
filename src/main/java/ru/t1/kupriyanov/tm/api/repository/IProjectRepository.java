package ru.t1.kupriyanov.tm.api.repository;

import ru.t1.kupriyanov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

}

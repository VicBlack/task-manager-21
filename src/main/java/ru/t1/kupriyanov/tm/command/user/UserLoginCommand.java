package ru.t1.kupriyanov.tm.command.user;

import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private final String NAME = "login";

    private final String DESCRIPTION = "user login";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

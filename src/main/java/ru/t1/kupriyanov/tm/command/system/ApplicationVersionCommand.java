package ru.t1.kupriyanov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.17.0");
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show version.";
    }

}
